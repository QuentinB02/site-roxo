---
category:
- Data Visualization
date: "2019-12-23T20:56:42+06:00"
image: images/projects/quality.jpg
project_images:
- images/projects/project-details-image-one.jpg
- images/projects/project-details-image-two.jpg
title: Disturbance Analysis on a Factory Line
type: portfolio
custom_url: disturbance_analysis.html
---
