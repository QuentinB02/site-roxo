---
category:
- Applied Machine Learning
date: "2019-12-23T20:56:42+06:00"
image: images/projects/nlp.jpg
project_images:
- images/projects/project-details-image-one.jpg
- images/projects/project-details-image-two.jpg
title: Natural Language Processing / Airline Reviews
type: portfolio
custom_url: airline_reviews.html
---
